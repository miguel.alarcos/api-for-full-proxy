FROM node:10-alpine

WORKDIR /usr/src/app

COPY package*.json ./

RUN yarn

RUN yarn global add jest

COPY . .

CMD [ "node", "index.js" ]